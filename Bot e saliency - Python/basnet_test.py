import os
from skimage import io
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms#, utils
from matplotlib import pyplot as plt
import png
import numpy as np
from PIL import Image
import glob
from skimage.measure import label
import data_loader as dl
import morphsnakes as ms
from model import BASNet
from imageio import imread
import matplotlib
from skimage import color
matplotlib.use('TkAgg')




def visual_callback_2d(background, fig=None):
    """
    Returns a callback than can be passed as the argument `iter_callback`
    of `morphological_geodesic_active_contour` and
    `morphological_chan_vese` for visualizing the evolution
    of the levelsets. Only works for 2D images.

    Parameters
    ----------
    background : (M, N) array
        Image to be plotted as the background of the visual evolution.
    fig : matplotlib.figure.Figure
        Figure where results will be drawn. If not given, a new figure
        will be created.

    Returns
    -------
    callback : Python function
        A function that receives a levelset and updates the current plot
        accordingly. This can be passed as the `iter_callback` argument of
        `morphological_geodesic_active_contour` and
        `morphological_chan_vese`.

    """

    # Prepare the visual environment.
    if fig is None:
        fig = plt.figure()
    fig.clf()
    ax1 = fig.add_subplot(1, 2, 1)
    ax1.imshow(background, cmap=plt.cm.gray)

    ax2 = fig.add_subplot(1, 2, 2)
    ax_u = ax2.imshow(np.zeros_like(background), vmin=0, vmax=1)
    plt.pause(0.001)

    def callback(levelset):

        if ax1.collections:
            del ax1.collections[0]
        ax1.contour(levelset, [0.5], colors='r')
        ax_u.set_data(levelset)
        fig.canvas.draw()
        plt.pause(0.001)

    return callback

def normPRED(d):
	ma = torch.max(d)
	mi = torch.min(d)

	dn = (d-mi)/(ma-mi)

	return dn

def save_output(image_name,pred,d_dir):

	predict = pred
	predict = predict.squeeze()
	predict_np = predict.cpu().data.numpy()

	im = Image.fromarray(predict_np*255).convert('RGB')
	img_name = image_name.split("/")[-1]
	image = io.imread(image_name)
	imo = im.resize((image.shape[1],image.shape[0]),resample=Image.BILINEAR)

	pb_np = np.array(imo)

	aaa = img_name.split(".")
	bbb = aaa[0:-1]
	imidx = bbb[0]
	ccc = imidx.split("\\");
	imidx = ccc[1]
	for i in range(1,len(bbb)):
		imidx = imidx + "." + bbb[i]
	imo.save(d_dir+imidx+'.png')


def computeSaliency():
	# --------- 1. get image path and name ---------
	
	image_dir = './image/temp/'
	prediction_dir = './image/mask/'
	model_dir = './saved_models/basnet_bsi/basnet.pth'


	img_name_list = glob.glob(image_dir + '*.png')
	print(img_name_list)
    
	
	# --------- 2. dataloader ---------
	#1. dataload
	test_salobj_dataset = dl.SalObjDataset(img_name_list = img_name_list, lbl_name_list = [],transform=transforms.Compose([dl.RescaleT(256),dl.ToTensorLab(flag=0)]))
	test_salobj_dataloader = DataLoader(test_salobj_dataset, batch_size=1,shuffle=False,num_workers=1)
	# --------- 3. model define ---------
	print("...load BASNet...")
	net = BASNet(3,1)
	net.load_state_dict(torch.load(model_dir))
	if torch.cuda.is_available():
		net.cuda()
		print('Uso Cuda')
	net.eval()
	print("inferencing:")
	inputs_test = test_salobj_dataset[0]['image'].unsqueeze(0)
	inputs_test = inputs_test.type(torch.FloatTensor)
	
	if torch.cuda.is_available():
		inputs_test = Variable(inputs_test.cuda())
	else:
		inputs_test = Variable(inputs_test)
	
	d1,d2,d3,d4,d5,d6,d7,d8 = net(inputs_test)
	
		# normalization
	pred = d2[:,0,:,:]
	pred = normPRED(pred)
	
	save_output(img_name_list[0],pred,prediction_dir)
	"""img = color.rgb2gray(imread(img_name_list[0]))
	gimg = ms.inverse_gaussian_gradient(img, alpha=1000, sigma=5)
	predict = pred
	predict = predict.squeeze()
	predict_np = predict.cpu().data.numpy()
	init_ls = predict_np

	im = Image.fromarray(predict_np*255).convert('RGB')
	imo = im.resize((img.shape[1],img.shape[0]),resample=Image.BILINEAR)
	pb_np = np.array(imo)
	init_ls = pb_np[:,:,1]
	init_ls = init_ls > 100;
	labels = label(init_ls)
	init_ls = labels == np.argmax(np.bincount(labels.flat, weights=init_ls.flat))
	callback = visual_callback_2d(img)
	newMask = ms.morphological_geodesic_active_contour(gimg, iterations=500,
                                             init_level_set=init_ls,
                                             smoothing=1, threshold=0.3,
                                             balloon=1, iter_callback=callback)
        # save results to test_results folder
	if not os.path.exists(prediction_dir):
		os.makedirs(prediction_dir, exist_ok=True)
	newMask = newMask > 0;
	png.from_array(newMask, 'L').save("test_data\\test_results\\ris.png")
"""
	
	del d1,d2,d3,d4,d5,d6,d7,d8
