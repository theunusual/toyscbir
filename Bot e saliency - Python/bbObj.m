function numObj = bbObj(fileName)
    color = string(['#77AC30';'#ff0000';'#0000ff';'#8a2be2'; '#ffa500';'#ffb6c1';'#ffff00';'#808080'; '#000000'; '#ffffff']);
    % Verde, rosso, blu, viola, arancione, rosa, giallo, grigio,nero, bianco
    im=imread(strcat('image/input/', fileName, '.png'));
    bw = imread(strcat('image/mask/', fileName, '.png'));
    bw = bw(:,:,1);
    bw = bw > 10;
    bw = bwareaopen(bw, 2000);
    bw = imfill(bw, 'holes');
    info = regionprops(bw,'Boundingbox');
    
    figure, imshow(im);
    numObj = length(info);
    for i = 1:numObj
        BB = info(i).BoundingBox;
        rectangle('Position',[BB(1),BB(2),BB(3),BB(4)],'EdgeColor',color(i),'Linewidth',3); 
    end
    ax = gca;
    exportgraphics(ax,strcat('image/obj/', fileName, '.png'),'Resolution',300) 
    numObj = string(numObj);
    f = fopen('./numObj.txt', 'w');
        fprintf(f, numObj);
    fclose(f);
    newInfo = struct2cell(info);
    save('botBB', 'newInfo');
end
