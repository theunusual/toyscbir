imageBw = imageDatastore('datasetMask/train','FileExtensions','.png');
allFile = imageBw.Files;
listBB = [];
for i = 1:length(allFile)
    im = im2double(imread(strcat('..\Dataset\train\',string(i + 83685), '.jpg')));
    name = allFile(i);
    name = name {1};
    bw = imread(name);
    bw = bw(:,:,1);
    bw = bw > 10;
    bw = bwareaopen(bw, 2000);
    bw = imfill(bw, 'holes');
    labeledIm = bwlabel(bw);
    for k = 1:max(max(labeledIm))
        objMask = labeledIm == k;
        info = regionprops(objMask,'Boundingbox');
        BB = info(1).BoundingBox;
        subName = strcat(string(i + 83685), '_', string(k));
        listBB = [listBB; subName, BB];
        obj = im .* objMask;
        onlyObject = imcrop(obj, BB);
        imwrite(onlyObject, strcat('datasetMask/object/', subName,'.png'));
    end
end

