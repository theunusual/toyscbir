#!/usr/bin/env python
# pylint: disable=C0116,W0613
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import os
import time
import subprocess
from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, PollAnswerHandler, Filters, CallbackContext
import basnet_test
import warnings
warnings.filterwarnings("ignore")
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Ciao {user.mention_markdown_v2()}\! Sono un bot per i giocattoli! Inviami un immagine!')


def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def photoHandler(update: Update, context: CallbackContext) -> None:
    message = update.message
    chat_id = message.chat.id
    fileID = message.photo[-1].file_id
    ris = context.bot.sendPoll(chat_id, "Quale metodo vuoi usare?", ["Colore", "Texture", "Deep"], allows_multiple_answers = True, is_anonymous  = False)
    payload = {
        ris.poll.id: {
            "questions": ["Colore", "Texture", "Deep"],
            "chat_id": chat_id,
            "answers": 0,
            "fileID": fileID,
        }
    }
    context.bot_data.update(payload)


def ansHandler(update: Update, context: CallbackContext) -> None:
    print(context.bot_data)
    answer = update.poll_answer.option_ids
    poll_id = update.poll_answer.poll_id
    chat_id = context.bot_data[poll_id]["chat_id"]
    fileID = context.bot_data[poll_id]["fileID"]
    context.bot.send_message(
        chat_id, "Attendi..."
    )
    for f in os.listdir('./image/temp/'):
        os.remove(os.path.join('./image/temp/', f))
    with open("image/temp/"+fileID+".png", 'wb') as new_file:
        context.bot.get_file(fileID).download(out = new_file)
    with open("image/input/"+fileID+".png", 'wb') as new_file:
        context.bot.get_file(fileID).download(out = new_file)
    cur_dir = os.path.dirname(os.path.realpath(__file__))
    local_filename = cur_dir + "\\image\\input\\" + fileID + '.png'
    basnet_test.computeSaliency();
    photo = open(cur_dir +'\\image\\mask\\' + fileID+'.png', 'rb')
    context.bot.send_message(
        chat_id, "Questa è la maschera:"
    )
    context.bot.send_photo(chat_id, photo)
    matlab_cmd = '"C:\\Program Files\\MATLAB\\R2021a\\bin\\matlab.exe"'
    cmd = matlab_cmd + " -nodesktop -nosplash -wait -r \"addpath(\'" + cur_dir + "\'); similarityBot(\'" + fileID + "\'); quit\""
    print(cmd)
    subprocess.call(cmd,shell=True)
    obj = open(cur_dir +'\\image\\obj\\' + fileID+'.png', 'rb')
    context.bot.send_message(
        chat_id, "Questo è l'oggetto:"
    )
    context.bot.send_photo(chat_id, obj)
    sim = open(cur_dir +'\\image\\similarity\\' + fileID+'.png', 'rb')
    context.bot.send_message(
        chat_id, "Queste sono gli oggetti più simili:"
    )
    context.bot.send_photo(chat_id, sim)
    os.remove(cur_dir+'\\image\\temp\\' + fileID+'.png')
    

def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater("1947065716:AAGXh53dd0oqyMHW_Ra6oe4y4uPbPzEEQoo", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
       # on non command i.e message - echo the message on Telegram
    dispatcher.add_handler(MessageHandler(Filters.photo, photoHandler))
    dispatcher.add_handler(PollAnswerHandler(ansHandler))
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()