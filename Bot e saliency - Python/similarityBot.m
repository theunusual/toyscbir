function similarityBot(fileName, answer)
    listAns = string(split(answer, ', '));
    net = resnet50;
    sz=net.Layers(1).InputSize;
    layer='avg_pool'; 
    imOrig=imread(strcat('image/input/', fileName, '.png'));
    if (ismember('0', listAns))
        load("../feature/avgpool_rgb_im.mat");
        feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
        feat_te=[];
        x=imresize(double(imOrig),sz(1:2));
        feat_tmp=activations(net,x,layer,'OutputAs','rows');
        feat_te=[feat_te; feat_tmp];
        feat_te=feat_te./sqrt(sum(feat_te.^2,2));
        [Idx,D] = knnsearch(feat_tr, feat_te, 'K', 11, 'Distance', 'minkowski');
        imageTrain = imageDatastore(fullfile('../Dataset/train'),'FileExtensions','.jpg');
        fileTrain = imageTrain.Files;
        figure(1), clf
        subplot(3,4,1), imshow(imOrig)
        title('Immagine originale');
        for i = 1:11
            name =  fileTrain(Idx(i));
            name = name {1};
            lenName = length(name);
            name = name(lenName-8:lenName-4);
            im = imread('../Dataset/train/' + string(name) + '.jpg');
            subplot(3,4,i+1), imshow(im);
            
            title(strcat(string(name), ' (', string(D(i)),')'));
        end
        saveas(gcf, strcat('image/similarity/', fileName, '_1.png'));
    else
        color = string(['#77AC30';'#ff0000';'#0000ff';'#8a2be2'; '#ffa500';'#ffb6c1';'#ffff00';'#808080'; '#000000'; '#ffffff']);
        load('botBB.mat');
        load('listBB.mat');
        load("../feature/avgpool_rgb_obj.mat");
        feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
        bw = imread(strcat('image/mask/', fileName, '.png'));
        bw = bw(:,:,1);
        bw = bw > 10;
        bw = bwareaopen(bw, 2000);
        bw = imfill(bw, 'holes');
        obj = double(imOrig).*  bw;
        objCol = double(imOrig).*  bw;
        nFiltSize=8;
        nFiltRadius=1;
        filtR=generateRadialFilterLBP(nFiltSize, nFiltRadius);
        for k = 1:length(listAns)
            currAns = str2double(listAns(k));
            feat_te=[];
            BB = newInfo{currAns};
            onlyObject = imcrop(obj, BB);
            %onlyObject = repmat(onlyObject,1,1,3);
            figure, imshow(uint8(onlyObject));
            x=imresize(onlyObject,sz(1:2));
            feat_tmp=activations(net,x,layer,'OutputAs','rows');
            feat_te=[feat_te; feat_tmp];
            feat_te=feat_te./sqrt(sum(feat_te.^2,2));
            [Idx,D] = knnsearch(feat_tr, feat_te, 'K', 16672, 'Distance', 'cityblock');
            
            imageTrain = imageDatastore(fullfile('datasetMask\object'),'FileExtensions','.png');
            fileTrain = imageTrain.Files;
            figure(), clf
            subplot(3,4,1), imshow(imOrig)
            title('Immagine originale');
            rectangle('Position',[BB(1),BB(2),BB(3),BB(4)],'EdgeColor',color(currAns),'Linewidth',1); 
            for i = 1:11
                name =  fileTrain(Idx(i));
                name = name {1};
                name = erase(name, pwd);
                name = name(21:25);
                imAns = imread('../Dataset/train/' + string(name) + '.jpg');
                subplot(3,4,i+1), imshow(imAns);
                currBB = double(listBB(Idx(i),2:5));
                rectangle('Position',[currBB(1),currBB(2),currBB(3),currBB(4)],'EdgeColor',color(currAns),'Linewidth',1);
                title(strcat(string(name)));
            end
            saveas(gcf, strcat('image/similarity/', fileName, '_',string(k),'.png'));   
        end   
    end

end
