%% Pre
fileName = 'AgACAgQAAxkBAAIKsGEydCjbpJwU8U2OxnHAuWva2_z_AAK8tzEbaLiRUciqYortwD3AAQADAgADeAADIAQ';
answer = '1';
listAns = string(split(answer, ', '));
net = resnet50;
sz=net.Layers(1).InputSize;
layer='avg_pool'; 
imOrig=imread(strcat('image/input/', fileName, '.png'));
color = string(['#77AC30';'#ff0000';'#0000ff';'#8a2be2'; '#ffa500';'#ffb6c1';'#ffff00';'#808080'; '#000000'; '#ffffff']);
load('botBB.mat');
load('listBB.mat');
load("../feature/avgpool_rgb_obj.mat");
load("../feature/handcraft_im.mat");
feat_tr = double(feat_tr);
feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
feat_col = double(table2array(info_table(:, 2:13)));
feat_col=feat_col./sqrt(sum(feat_col.^2,2));
featTrain = [feat_tr, feat_col] ; 
% Da 1 a 2048 deep; da 2049 a a 2060 colore; da 2061 a 2070 LBPR; da 2071 a
% 2080 LBPG; da 2081 a 2090 LBPB; da 2091 a 2100 LBPIM; da 2100 a 2244 cedd
%% Maschera
bw = imread(strcat('image/mask/', fileName, '.png'));
bw = bw(:,:,1);
bw = bw > 10;
bw = bwareaopen(bw, 2000);
bw = imfill(bw, 'holes');
obj = double(imOrig).*  bw;
objCol = double(imOrig).*  bw;
feat_te=[];
BB = newInfo{1};
onlyObject = imcrop(obj, BB);

%% Deep
x=imresize(onlyObject,sz(1:2));
feat_te=double(activations(net,x,layer,'OutputAs','rows'));
feat_te=feat_te./sqrt(sum(feat_te.^2,2));
[IDXdeep,Ddeep] = knnsearch(featTrain(:,1:2048), feat_te, 'K', 16672, 'Distance', 'minkowski','P',1, 'SortIndices', true);
[~,sortIdx] = sort(IDXdeep,'ascend');
Ddeep = Ddeep(sortIdx);

[IDXdeep2,Ddeep2] = knnsearch(featTrain(:,1:2048), feat_te, 'K', 16672, 'Distance', 'minkowski','P',2, 'SortIndices', true);
[ascIdx,sortIdx] = sort(IDXdeep2,'ascend');
Ddeep2 = Ddeep2(sortIdx);
%% Colore
R = onlyObject(:,:,1);
R= double(R(:));
G = onlyObject(:,:,2);
G=double(G(:));
B = onlyObject(:,:,3);
B = double(B(:));
colorFeat = [mean(R) std(R) skewness(R) kurtosis(R) mean(G) std(G) skewness(G) kurtosis(G) mean(B) std(B) skewness(B) kurtosis(B)];
colorFeat=colorFeat./sqrt(sum(colorFeat.^2,2));
[IDXcol,Dcool] = knnsearch(featTrain(:,2049:2060), colorFeat, 'K', 16672, 'Distance', 'cityblock', 'SortIndices', true);
[ascIdx,sortIdx] = sort(IDXcol,'ascend');
Dcool = Dcool(sortIdx);
%% Similarity
%featTest = [feat_te, colorFeat];
%[Idx,D] = knnsearch(featTrain, featTest, 'K', 16672, 'Distance', 'cityblock', 'SortIndices', true);

newRank = [];
indexRank = [];
for i = 1:16672
    index1 = find(IDXdeep==i);
    index2 = find(IDXdeep2==i);
    indexRank = [indexRank; index1, index2];
    newRank = [newRank;index1+index2];
end

[newRankOrd,sortIdx] = sort(newRank,'ascend');
Idx= ascIdx(sortIdx);

imageTrain = imageDatastore(fullfile('datasetMask\object'),'FileExtensions','.png');
fileTrain = imageTrain.Files;
figure(), clf
subplot(3,4,1), imshow(imOrig)
title('Immagine originale');
rectangle('Position',[BB(1),BB(2),BB(3),BB(4)],'EdgeColor',color(1),'Linewidth',1); 
for i = 1:11
    name =  fileTrain(Idx(i));
    name = name {1};
    name = erase(name, pwd);
    name = name(21:25);
    imAns = imread('../Dataset/train/' + string(name) + '.jpg');
    subplot(3,4,i+1), imshow(imAns);
    currBB = double(listBB(Idx(i),2:5));
    rectangle('Position',[currBB(1),currBB(2),currBB(3),currBB(4)],'EdgeColor',color(1),'Linewidth',1);
    title(strcat(string(name), ':', string(newRank(Idx(i))),'(', string(find(IDXdeep==Idx(i))),' + ',string(find(IDXdeep2==Idx(i))),')'));
end 

