function similarityTest()
    net = resnet50;
    sz=net.Layers(1).InputSize;
    layer='avg_pool'; 
    load('listBB.mat');
    load("../feature/avgpool_rgb_obj.mat");
    imdsAll = imageDatastore(fullfile('datasetMask/test'),'FileExtensions','.png', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
    allFiles = imdsAll.Files;
    for k = 1:length(allFiles)
        feat_te=[];
        name = allFiles(k);
        name = name {1};
        name = split(name,'\');
        name = name(size(name,1));
        name = name {1};
        fileName = name(1: length(name)-4);
        imOrig=imread(strcat('../Dataset/test/', fileName, '.jpg'));
        bw = imread(strcat('datasetMask/test/', fileName, '.png'));
        bw = bw(:,:,1);
        bw = bw > 10;
        bw = bwareaopen(bw, 2000);
        bw = imfill(bw, 'holes');
        obj = double(imOrig).*  bw;
        info = regionprops(bw,'Boundingbox');
        BB = info(1).BoundingBox;
        onlyObject = imcrop(obj, BB);
        x=imresize(onlyObject,sz(1:2));
        feat_tmp=activations(net,x,layer,'OutputAs','rows');
        feat_te=[feat_te; feat_tmp];


        feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
        feat_te=feat_te./sqrt(sum(feat_te.^2,2));

        Idx = [];
        [Idx,D] = knnsearch(feat_tr, feat_te, 'K', 11, 'Distance', 'minkowski');
        imageTrain = imageDatastore(fullfile('datasetMask\object'),'FileExtensions','.png');
        fileTrain = imageTrain.Files;
        figure(1), clf
        subplot(3,4,1), imshow(imOrig)
        title('Immagine originale');
        for i = 1:11
            name =  fileTrain(Idx(i));
            name = name {1};
            name = erase(name, pwd);
            name = name(21:25);
            im = imread('../dataset/train/' + string(name) + '.jpg');
            subplot(3,4,i+1), imshow(im);
            BB = double(listBB(Idx(i),2:5));
            rectangle('Position',[BB(1),BB(2),BB(3),BB(4)],'EdgeColor','#77AC30','Linewidth',1);
            title(strcat(string(name), ' (', string(D(i)),')'));
        end
        saveas(gcf, strcat('image/similarity/', fileName, '.png'));
    end
end
