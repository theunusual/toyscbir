function testSimilarity()
    net = resnet50;
    sz=net.Layers(1).InputSize;
    layer='avg_pool'; 
    load('listBB.mat');
    load("../feature/avgpool_rgb_obj.mat");
    
    imdsAll = imageDatastore(fullfile('datasetMask/train'),'FileExtensions','.png', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
    allFiles = imdsAll.Files;
    for i= 1:length(allFiles)
        disp(i)
        feat_te=[];
        name = allFiles(i);
        name = name {1};
        name = erase(name, pwd);
        name = split(name,'\');
        name = name(size(name,1));
        name = name {1};
        fileName = name(1: length(name)-4);
        imOrig=imread(strcat('../Dataset/train/', fileName, '.jpg'));
        bw = imread(strcat('datasetMask/train/', fileName, '.png'));
        bw = bw(:,:,1);
        bw = bw > 10;
        bw = bwareaopen(bw, 2000);
        bw = imfill(bw, 'holes');
        
        L = bwlabel(bw);
        if max(max(L)) == 1
            obj = double(imOrig).*  bw;
            info = regionprops(bw,'Boundingbox');
            BB = info(1).BoundingBox;
            onlyObject = imcrop(obj, BB);
            %imwrite(uint8(onlyObject), strcat('image/obj/', fileName, '.png'));
            x=imresize(onlyObject,sz(1:2));
            feat_tmp=activations(net,x,layer,'OutputAs','rows');
            feat_te=[feat_te; feat_tmp];

            feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
            feat_te=feat_te./sqrt(sum(feat_te.^2,2));

            [Idx,D] = knnsearch(feat_tr, feat_te, 'K', 11, 'Distance', 'minkowski');
            %imageTrain = imageDatastore(fullfile('datasetMask\object'),'FileExtensions','.png');
            %fileTrain = imageTrain.Files;
            fileTrain = listBB(:,1);
            %close all
            %figure, clf
            %subplot(3,4,1), imshow(uint8(imOrig))
            %title('Immagine originale');

            f = fopen('./risultati.txt', 'a');
            fileNameF = string(fileName)+ '.jpg\n';
            fprintf(f, fileNameF);

            for j = 1:11
                name = fileTrain(Idx(j));
                nameC = char(name);
                nameC = nameC(1:size(nameC,2)-2);
                name =string(nameC);
                %im = imread('../Dataset/train/' + string(name) + '.jpg');
                nameF = name + '.jpg\n';
                fprintf(f, nameF);

                %subplot(3,4,j+1), imshow(im);
                %BB = double(listBB(Idx(j),2:5));
                %rectangle('Position',[BB(1),BB(2),BB(3),BB(4)],'EdgeColor','g','Linewidth',1);
                %title(strcat(string(name), ' (', string(D(j)),')'));
            end
            fclose(f);

            %saveas(gcf, strcat('image/similarity/', fileName, '.png'));
        end
    end
        
    
    
end
