D='C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\toyscbir\Bot e saliency - Python\datasetMask\object';
imgFolder = fullfile(D);
filePattern = fullfile(imgFolder, '*.png');
jpegFiles = dir(filePattern);
info_table = cell2table(cell(0,18),'VariableNames',{'fname','RedMean','RedStd','RedSkew','RedKurtosis','GreenMean','GreenStd','GreenSkew','GreenKurtosis','BlueMean','BlueStd','BlueSkew','BlueKurtosis', 'lbpR', 'lbpG', 'lbpB', 'lbpIm', 'CEDD'});
nFiltSize=8;
nFiltRadius=1;
filtR=generateRadialFilterLBP(nFiltSize, nFiltRadius);
for k = 1:length(jpegFiles)
  baseFileName = jpegFiles(k).name;
  fullFileName = fullfile(imgFolder, baseFileName);
  disp(k);
  img = imread(fullFileName);
  cedd = f_computeCEDD(img);
  R = img(:,:,1);
  lbpR = extractLBPFeatures(R, 'Upright',false);
  R= double(R(:));
  G = img(:,:,2);
  lbpG = extractLBPFeatures(G, 'Upright',false);
  G=double(G(:));
  B = img(:,:,3);
  lbpB = extractLBPFeatures(B, 'Upright',false);
  B = double(B(:));
  gray=rgb2gray(img);
  lbpIm = extractLBPFeatures(gray, 'Upright',false);
  ImgVals = [mean(R) std(R) skewness(R) kurtosis(R) mean(G) std(G) skewness(G) kurtosis(G) mean(B) std(B) skewness(B) kurtosis(B) lbpR lbpG lbpB lbpIm cedd];
  new_row = {fullFileName,ImgVals(1),ImgVals(2),ImgVals(3),ImgVals(4),ImgVals(5),ImgVals(6),ImgVals(7),ImgVals(8),ImgVals(9),ImgVals(10),ImgVals(11),ImgVals(12), ImgVals(13:22),ImgVals(23:32),ImgVals(33:42),ImgVals(43:52), ImgVals(53:196)};
  info_table=[info_table;new_row];
end
save('colorlbp_rgb_obj', 'info_table');