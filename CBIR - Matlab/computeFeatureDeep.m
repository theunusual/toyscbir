%% Preparazione rete
rng(1);
net = resnet50;
% analyzeNetwork(net)
sz=net.Layers(1).InputSize;
layer='avg_pool'; % <---------- layer
%analyzeNetwork(net)
imdsTrain = imageDatastore(fullfile('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\Progetto\train'),'FileExtensions','.jpg');
sizeDataStore = size(imdsTrain.Files);
fileTrain = imdsTrain.Files;

%% Feature extraction per train
tic
feat_tr = [];
labels_tr = [];
info_table = cell2table(cell(0,2),'VariableNames',{'fname','featDeep'});
for i = 1:length(fileTrain)
    disp(i)
    im = double(imread(fileTrain{i}));
    im=imresize(im,sz(1:2));
    feat_tmp=activations(net,im,layer,'OutputAs','rows','ExecutionEnvironment', 'cpu');
    new_row = {fileTrain{i},feat_tmp};
    info_table=[info_table;new_row];
end
toc
writetable(info_table,'featureDeep.csv');