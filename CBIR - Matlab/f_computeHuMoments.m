function phi  = f_computeHuMoments(F)
if (ndims(F)~=2) | issparse(F) | ~isreal(F) | ~(isnumeric(F) | ...
            islogical(F))
        error(['F must be a "-D, real, nonparse, numeric or logical'...
            'matrix']);
end
    
  F = double(F);
  phi = f_compute_phi(f_compute_e(f_compute_m(F)));
  phi = cell2mat(struct2cell(phi))';
  