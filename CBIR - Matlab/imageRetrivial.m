dbFeature = readtable('feature.csv');
queryImage = imread('queryImage/83701_3.jpg');
R = queryImage(:,:,1);
R= double(R(:));
G = queryImage(:,:,2);
G=double(G(:));
B = queryImage(:,:,3);
B = double(B(:));
gray=rgb2gray(queryImage);
aq=extractLBPFeatures(gray,'Upright',false);
featTamura = tamura_features(queryImage);
QueryVals = [mean(R) std(R) skewness(R) kurtosis(R) mean(G) std(G) skewness(G) kurtosis(G) mean(B) std(B) skewness(B) kurtosis(B) aq];
QueryVals = double(QueryVals);
QueryVals = QueryVals(1:22);
distTable = cell2table(cell(0,2),'VariableNames',{'fname','cityblock'});
listFiles = dbFeature(:,1);
numFiles = size(listFiles);
numFiles = numFiles(1);
tic
for k = 1:numFiles
  nameFile = listFiles(k,:);
  nameFile = nameFile{1,1};
  nameFile = nameFile{1};
  ImgVals = table2array(dbFeature(k, 2:23));
  dist = pdist2(ImgVals, QueryVals,'cityblock');
  new_row = {nameFile,dist};
  distTable=[distTable;new_row];
end
toc
distTable = sortrows(distTable,'cityblock');
subplot(332)
imshow(queryImage); title('Query Image'); 
for i=1:6
    withPath = char(distTable(i,1).fname);
    name = strcat(withPath(1: 110), 'toyscbir\Dataset', withPath(119: 134));
    im1=imread(name);
    [as,sd,fg] = fileparts(char(distTable(i,1).fname));
    dist = string(distTable(i,2).cityblock);
    subplot(3,3,i+3)
    imshow(im1);
    title(strcat(sd, '(', dist, ')'));
end  