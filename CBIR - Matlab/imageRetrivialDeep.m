%% Preparazione rete
rng(1);
net = resnet50;
% analyzeNetwork(net)
sz=net.Layers(1).InputSize;
layer='avg_pool'; % <---------- layer
imdsTest = imageDatastore(fullfile('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\Progetto\Content-Based-Image-Retrieval-master\queryImage'),'FileExtensions','.jpg');
fileTest = imdsTest.Files;
%% Test
dbFeature = readtable('featureDeep.csv');
queryImage = im2double(imread('queryImage/02.jpg'));
queryImage=imresize(queryImage,sz(1:2));
QueryVals=activations(net,queryImage,layer,'OutputAs','rows','ExecutionEnvironment', 'cpu');
distTable = cell2table(cell(0,2),'VariableNames',{'fname','euclidean'});
listFiles = dbFeature(:,1);
numFiles = size(listFiles);
numFiles = numFiles(1);
tic
for k = 1:numFiles
  nameFile = listFiles(k,:);
  nameFile = nameFile{1,1};
  nameFile = nameFile{1};
  ImgVals = table2array(dbFeature(k, 2:2049));
  dist = sqrt(sum((QueryVals - ImgVals) .^ 2));
  new_row = {nameFile,dist};
  distTable=[distTable;new_row];
end
toc
distTable = sortrows(distTable,'euclidean');
subplot(332)
imshow(queryImage); title('Query Image'); 
for i=1:6
    im1=imread(char(distTable(i,1).fname));
    [as,sd,fg] = fileparts(char(distTable(i,1).fname));
    subplot(3,3,i+3)
    imshow(im1);
    title(sd);
end  