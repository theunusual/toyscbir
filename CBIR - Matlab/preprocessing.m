img = imread('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\Progetto\train\/83696.jpg');
gray = rgb2gray(img);
patchVar = std2(gray)^2;
DoS = 2*patchVar;
filt1 = imbilatfilt(gray,DoS, 1);
filt2 = imbilatfilt(gray,DoS, 2);
figure, imshow(filt1);
figure, imshow(img);
img = filt2;
% create gaussian filter
h = fspecial('gaussian',5,2.5);
% blur the image
blurred_img = imfilter(img,h);
figure, imshow(blurred_img);
% subtract blurred image from original
diff_img = img - blurred_img;
figure, imshow(diff_img);
% add difference to the original image
highboost_img = img + 5*diff_img;
figure, imshow(highboost_img);
