I = imread('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\Progetto\train\/83696.jpg');
J = imread('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\Progetto\train\83702.jpg');

Ia = single(rgb2gray(I)); % Conversion to single is recommended
Ia = imresize(Ia, [224, 224]);
Ib = single(rgb2gray(J)); % in the documentation
Ib = imresize(Ib, [224, 224]);

[fa D1] = vl_sift(Ia);
[fb D2] = vl_sift(Ib);

% Where 1.5 = ratio between euclidean distance of NN2/NN1
[matches scores] = vl_ubcmatch(D1,D2, 1.5); 

[drop, perm] = sort(scores, 'descend') ;
matches = matches(:, perm) ;
scores  = scores(perm) ;

figure(1) ; clf ;
imagesc(cat(2, Ia, Ib)) ;
axis image off ;
vl_demo_print('sift_match_1', 1) ;

figure(2) ; clf ;
imagesc(cat(2, Ia, Ib)) ;

xa = fa(1,matches(1,:)) ;
xb = fb(1,matches(2,:)) + size(Ia,2) ;
ya = fa(2,matches(1,:)) ;
yb = fb(2,matches(2,:)) ;

hold on ;
h = line([xa ; xb], [ya ; yb]) ;
set(h,'linewidth', 1, 'color', 'b') ;

vl_plotframe(fa(:,matches(1,:))) ;
fb(1,:) = fb(1,:) + size(Ia,2) ;
vl_plotframe(fb(:,matches(2,:))) ;
axis image off ;

vl_demo_print('sift_match_2', 1) ;
