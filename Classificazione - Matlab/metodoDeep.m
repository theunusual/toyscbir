%% Step 1: preprocessing
%% Step 2.1: estrazione oggetti per train
imdsAll = imageDatastore(fullfile('train'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
allFiles = imdsAll.Files;
allLabels = imdsAll.Labels;

for i = 1:208
    pathFile = allFiles(i);
    pathFile = pathFile{1};
    imOrig = imread(pathFile);
    lenName = length(pathFile);
    nameFile = pathFile(lenName-8:lenName-4);
    path = pathFile(1:lenName-10);
    mask = imread(strcat("U-2-Net-master\test_data\u2net_results\",nameFile,'.png'));
    mask = mask(:,:,1);
    mask = mask > 0;
    se = strel('disk',5);
    highMask = imclose(mask, se);
    highMask = imfill(highMask, 'holes');
    objectMask = bwareafilt(highMask,1);
    imMasked = objectMask .* im2double(imOrig);
    info = regionprops(objectMask,'Boundingbox');
    BB = info(1).BoundingBox;
    onlyObject = imcrop(imMasked, BB);
    onlyObject = imresize(onlyObject, [224 224]);
    imwrite(onlyObject, strcat('newTrain\', string(allLabels(i)), '\',nameFile, 'BB.jpg'));
end
%% Step 2.2: estrazione oggetti per test
imdsAll = imageDatastore(fullfile('test'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
allFiles = imdsAll.Files;
for i = 1:length(allFiles)
    pathFile = allFiles(i);
    pathFile = pathFile{1};
    imOrig = imread(pathFile);
    lenName = length(pathFile);
    nameFile = pathFile(lenName-5:lenName-4);
    path = pathFile(1:lenName-12);
    mask = imread(strcat("U-2-Net-master\test_data\u2net_resultsNew\",nameFile,'.png'));
    mask = mask(:,:,1);
    mask = mask > 0;
    se = strel('disk',5);
    highMask = imclose(mask, se);
    highMask = imfill(highMask, 'holes');
    objectMask = bwareafilt(highMask,1);
    imMasked = objectMask .* im2double(imOrig);
    info = regionprops(objectMask,'Boundingbox');
    BB = info(1).BoundingBox;
    onlyObject = imcrop(imMasked, BB);
    onlyObject = imresize(onlyObject, [224 224]);
    imwrite(onlyObject, strcat('newTest\',nameFile, 'BB.jpg'));
end
%% Step 3: data augmentation
imdsNew = imageDatastore(fullfile('newTrain'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
%[imdsTrainingSet, imdsValidationSet] = splitEachLabel(imdsNew, 0.7);
imdsTrainingSet = imdsNew;
imageAugmenter = imageDataAugmenter( ...
    'FillValue', 0, ...
    'RandXReflection', true, ...
    'RandYReflection', true, ...
    'RandRotation', @randomRotation, ...
    'RandScale', [1 1.5],...
    'RandYShear', [0 5], ...
    'RandXShear', [0 5], ...
    'RandXTranslation', [-5 5], ...
    'RandYTranslation', [-5 5]);
imageSize = [224 224 3];
label = unique(imdsTrainingSet.Labels);
for i = 1:length(label)
    class = label(i);
    disp(class)
    subset = imdsTrainingSet.Files(imdsTrainingSet.Labels == string(class));
    [inst,dimension] = size(subset);
    imdsSub = imageDatastore(subset,'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
    numAug = 0;
    while inst < 60
        augImds = augmentedImageDatastore(imageSize,imdsSub,'DataAugmentation',imageAugmenter);
        listName = imdsSub.Files;
        sizeImds = size(imdsSub.Files);
        for j = 1:sizeImds
            if inst < 60
                nameFile = listName(j);
                nameFile = nameFile{1};
                lenName = length(nameFile);
                onlyName =  extractBetween(nameFile,lenName-10,lenName-4);
                data = readByIndex(augImds,j);
                augIm = cell2mat(data{1,1});
                imwrite(augIm, strcat('newTrain\', string(class), '\',onlyName, '_augm', string(numAug),'.jpg'));
                inst = inst + 1;
            end
        end
        numAug = numAug + 1;
    end
end
%% Step 4.1: feature extraction, preparazione
rng(1);
net = resnet50;
% analyzeNetwork(net)
sz=net.Layers(1).InputSize;
layer='res5c_branch2c'; % <---------- layer
%analyzeNetwork(net)
imdsTrain = imageDatastore(fullfile('newTrain'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
sizeDataStore = size(imdsTrain.Files);
fileTrain = imdsTrain.Files;
labelTrain = imdsTrain.Labels;
imdsTest = imageDatastore(fullfile('test'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
fileTest = imdsTest.Files;
labelTest = imdsTest.Labels;
uniqueLabel = unique(labelTrain);

%% Step 4.2: feature extraction per train
tic
feat_tr = [];
labels_tr = [];
for j = 1:length(uniqueLabel)
    class = string(uniqueLabel(j));
    disp(class + "("+j+")");
    subtable = fileTrain(labelTrain == string(class));
    [inst,dimension] = size(subtable);
    for i = 1: inst
        im = im2double(imread(subtable{i}));
        sizeIm = size(im);
        if (min(sizeIm == [224, 224, 3]) == 0)
            im=imresize(im,sz(1:2));
        end
        feat_tmp=activations(net,im,layer,'OutputAs','rows','ExecutionEnvironment', 'cpu');
        singFeat = gather(feat_tmp);
        feat_tr=[feat_tr; singFeat];
        labels_tr=[labels_tr; class];
    end
end
toc

%% Step 4.3: feature extraction per test

tic
feat_te = [];
labels_te = [];
imdsTest = imageDatastore('newTest','FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
fileTest = imdsTest.Files;
for i = 1: length(fileTest)
    name = fileTest(i);
    name = name{1};
    im = im2double(imread(name));
    sizeIm = size(im);
    if (min(sizeIm == [224, 224, 3]) == 0)
        im=imresize(im,sz(1:2));
    end
    feat_tmp=activations(net,im,layer,'OutputAs','rows', 'ExecutionEnvironment', 'cpu');
    singFeat = gather(feat_tmp);
    feat_te=[feat_te; singFeat];
    labels_te=[labels_te; class];
end
toc
%% Step 4.4: normalizzazione feature
disp('Normalizzazione');
feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
feat_te=feat_te./sqrt(sum(feat_te.^2,2));

%% Step 5.1: classificazione con NN
D=pdist2(feat_te,feat_tr, 'cosine');
[dummy,idx_pred_te]=min(D,[],2);
lab_pred_te=labels_tr(idx_pred_te);



for i = 1: length(fileTest)
    nameIm = fileTest(i);
    nameIm = nameIm {1};
    im = imread(nameIm);
    figure(1);
    subplot(1,2,1);
    imshow(imresize(im,sz(1:2)));
    title(nameIm(138:length(nameIm)));
    mostSim = fileTrain(idx_pred_te(i));
    imMostSim = imread(mostSim{1});
    subplot(1,2,2);
    imshow(imMostSim);
    title(lab_pred_te(i));
    saveas(gcf,'output/figure'+string(i)+'_1.jpg');
end
acc=numel(find(lab_pred_te==labels_te))/numel(labels_te);

%% Step 5.2: classificazione knn
mdl = fitcknn(feat_tr,labels_tr,'NumNeighbors',7,'Standardize',1);
label_pred = predict(mdl,feat_te);