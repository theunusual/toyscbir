%% Step 0: preparazione
rng(1);
net = resnet50;
sz=net.Layers(1).InputSize;

%% Step 2.1: estrazione oggetti per train
% IMPORTANTE: quando va in errore dicendo che il file non esiste, ha finito
imdsAll = imageDatastore(fullfile('train'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
allFiles = imdsAll.Files;
allLabels = imdsAll.Labels;

for i = 1:length(allFiles)
    pathFile = allFiles(i);
    pathFile = pathFile{1};
    imOrig = imread(pathFile);
    noPwd = erase(pathFile,strcat(pwd,'\train\'));
    posSlash = strfind(noPwd, '\');
    class = noPwd(1:posSlash-1);
    nameFile = noPwd(posSlash + 1:length(noPwd)-4);
    mask = imread(strcat("U-2-Net-master\test_data\u2net_results\",nameFile,'.png'));
    mask = mask(:,:,1);
    mask = mask > 0;
    se = strel('disk',5);
    highMask = imclose(mask, se);
    highMask = imfill(highMask, 'holes');
    objectMask = bwareafilt(highMask,1);
    imMasked = objectMask .* im2double(imOrig);
    info = regionprops(objectMask,'Boundingbox');
    BB = info(1).BoundingBox;
    onlyObject = imcrop(imMasked, BB);
    onlyObject = imresize(onlyObject, sz(1:2));
    imwrite(onlyObject, strcat('newTrain\',class, '\',nameFile, 'BB.jpg'));
end

%% Step 2.2: estrazione oggetti per test
imdsAll = imageDatastore(fullfile('test'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
allFiles = imdsAll.Files;
for i = 1:length(allFiles)
    pathFile = allFiles(i);
    pathFile = pathFile{1};
    imOrig = imread(pathFile);
    noPwd = erase(pathFile,strcat(pwd,'\test\'));
    nameFile = noPwd(1:length(noPwd)-4);
    mask = imread(strcat("U-2-Net-master\test_data\u2net_resultsNew\",nameFile,'.png'));
    mask = mask(:,:,1);
    mask = mask > 0;
    se = strel('disk',5);
    highMask = imclose(mask, se);
    highMask = imfill(highMask, 'holes');
    objectMask = bwareafilt(highMask,1);
    imMasked = objectMask .* im2double(imOrig);
    info = regionprops(objectMask,'Boundingbox');
    BB = info(1).BoundingBox;
    onlyObject = imcrop(imMasked, BB);
    onlyObject = imresize(onlyObject, sz(1:2));
    imwrite(onlyObject, strcat('newTest\',nameFile, 'BB.jpg'));
end
%% Step 3: data augmentation
imdsForAug = imageDatastore(fullfile('newTrain'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
imageAugmenter = imageDataAugmenter( ...
    'FillValue', 0, ...
    'RandXReflection', true, ...
    'RandYReflection', true, ...
    'RandRotation', @randomRotation, ...
    'RandScale', [1 1.5],...
    'RandYShear', [0 5], ...
    'RandXShear', [0 5], ...
    'RandXTranslation', [-5 5], ...
    'RandYTranslation', [-5 5]);
label = unique(imdsForAug.Labels);
for i = 1:length(label)
    class = label(i);
    subset = imdsForAug.Files(imdsForAug.Labels == string(class));
    [inst,dimension] = size(subset);
    imdsSub = imageDatastore(subset,'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
    numAug = 0;
    while inst < 60
        augImds = augmentedImageDatastore(sz,imdsSub,'DataAugmentation',imageAugmenter);
        listName = imdsSub.Files;
        sizeImds = size(imdsSub.Files);
        for j = 1:sizeImds
            if inst < 60
                pathFile = listName(j);
                pathFile = pathFile{1};
                noPwd = erase(pathFile,strcat(pwd,'\newTrain\'));
                posSlash = strfind(noPwd, '\');
                nameFile = noPwd(posSlash + 1:length(noPwd)-4);
                data = readByIndex(augImds,j);
                augIm = cell2mat(data{1,1});
                imwrite(augIm, strcat('newTrain\', string(class), '\',nameFile, '_augm', string(numAug),'.jpg'));
                inst = inst + 1;
            end
        end
        numAug = numAug + 1;
    end
end
%% Opzione 1: finetuning con alexnet
net = alexnet;
numClasses = 15;
layersTransfer=net.Layers(1:end-3);
layers=[
    layersTransfer
    fullyConnectedLayer(numClasses,'weightLearnRateFactor',20,...
    'BiasLearnRateFactor',20)
    softmaxLayer
    classificationLayer];
imdsTrain = imageDatastore('newTrain','FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
imdsTest = imageDatastore('newTest','FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
options = trainingOptions('sgdm', ...
    'MaxEpochs',20,...
    'InitialLearnRate',1e-4, ...
    'Verbose',false, ...
    'MiniBatchSize', 1, ...
    'Plots','training-progress');
netTransfer=trainNetwork(imdsTrain,layers,options);
[lab_pred_te,scores]=classify(netTransfer,imdsTest);
fileTest = imdsTest.Files;
for i = 1: length(fileTest)
    nameIm = fileTest(i);
    nameIm = nameIm {1};
    im = imread(nameIm);
    figure(1);
    imshow(im);
    title(strcat(string(lab_pred_te(i)), " - ", string(max(scores(i,:)))));
    saveas(gcf,'outputTuning/figure'+string(i)+'_alexnet.jpg');
end

%% Opzione 2: FineTuning con Resenet50
csvTest = readtable('labelTest.csv');
filesTest = table2array(csvTest(:,1));
nameTest = strcat(pwd,'\newTest\', string(filesTest), 'BB.jpg');
labelTest = categorical(table2array(csvTest(:,2)));
imdsTrain = imageDatastore('newTrain','FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
imdsTest = imageDatastore(nameTest,'FileExtensions','.jpg', 'labels', labelTest);
if isa(net,'SeriesNetwork') 
  lgraph = layerGraph(net.Layers); 
else
  lgraph = layerGraph(net);
end 
[learnableLayer,classLayer] = findLayersToReplace(lgraph);
numClasses = 15;
if isa(learnableLayer,'nnet.cnn.layer.FullyConnectedLayer')
    newLearnableLayer = fullyConnectedLayer(numClasses, ...
        'Name','new_fc', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);
    
elseif isa(learnableLayer,'nnet.cnn.layer.Convolution2DLayer')
    newLearnableLayer = convolution2dLayer(1,numClasses, ...
        'Name','new_conv', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);
end
lgraph = replaceLayer(lgraph,learnableLayer.Name,newLearnableLayer);
newClassLayer = classificationLayer('Name','new_classoutput');
lgraph = replaceLayer(lgraph,classLayer.Name,newClassLayer);
layers = lgraph.Layers;
connections = lgraph.Connections;

layers(1:174) = freezeWeights(layers(1:174));
lgraph = createLgraphUsingConnections(layers,connections);
miniBatchSize = 10;
valFrequency = floor(numel(imdsTrain.Files)/miniBatchSize);
options = trainingOptions('sgdm', ...
    'MiniBatchSize',miniBatchSize, ...
    'MaxEpochs',20, ...
    'InitialLearnRate',3e-4, ...
    'Shuffle','every-epoch', ...
    'ValidationData',imdsTest, ...
    'ValidationFrequency',valFrequency, ...
    'Verbose',false, ...
    'Plots','training-progress');
net = trainNetwork(imdsTrain,lgraph,options);
[YPred,probs] = classify(net,imdsTest);
accuracy = mean(YPred == imdsTest.Labels);
%% Stampa calssificazione
fileTest = imdsTest.Files;
for i = 1: length(fileTest)
    nameIm = fileTest(i);
    nameIm = nameIm {1};
    im = imread(nameIm);
    figure(1);
    imshow(im);
    title(strcat(string(YPred(i)), " - ", string(max(probs(i,:)))));
    saveas(gcf,'output/figure'+string(i)+'_resnet50.jpg');
end

%%  Similarity tramite fine tuning per train
csvTest = readtable('labelTest.csv');
filesTest = table2array(csvTest(:,1));
nameTest = strcat(pwd,'\newTest\', string(filesTest), 'BB.jpg');
labelTest = categorical(table2array(csvTest(:,2)));
imdsTrain = imageDatastore('newTrain','FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
imdsTest = imageDatastore(nameTest,'FileExtensions','.jpg', 'labels', labelTest);
numClasses = 15;
if isa(net,'SeriesNetwork') 
  lgraph = layerGraph(net.Layers); 
else
  lgraph = layerGraph(net);
end 
layers = lgraph.Layers;
connections = lgraph.Connections;
layersTransfer=layers(1:end-3);
newLearnableLayer = fullyConnectedLayer(1024, ...
        'Name','stratoInPiu', ...
        'WeightLearnRateFactor',10, ...
        'BiasLearnRateFactor',10);
newLayers=[
    layersTransfer
    newLearnableLayer
    reluLayer('Name', 'reluConn')
    fullyConnectedLayer(numClasses,'weightLearnRateFactor',20,...
    'BiasLearnRateFactor',20, 'Name', "fullyConn")
    softmaxLayer('Name', 'softMaxConn')
    classificationLayer('Name', 'outputLayer')];
connTransfer=connections(1:end-3, :);
connTransfer = [connTransfer;{'avg_pool', 'stratoInPiu'}];
connTransfer = [connTransfer;{'stratoInPiu', 'reluConn'}];
connTransfer = [connTransfer;{'reluConn', 'fullyConn'}];
connTransfer = [connTransfer;{'fullyConn', 'softMaxConn'}];
newConn = [connTransfer;{'softMaxConn', 'outputLayer'}];
layers(1:10) = freezeWeights(layers(1:10));
lgraph = createLgraphUsingConnections(newLayers,newConn);
miniBatchSize = 10;
valFrequency = floor(numel(imdsTrain.Files)/miniBatchSize);
options = trainingOptions('sgdm', ...
    'MiniBatchSize',miniBatchSize, ...
    'MaxEpochs',3, ...
    'InitialLearnRate',3e-4, ...
    'Shuffle','every-epoch', ...
    'ValidationData',imdsTest, ...
    'ValidationFrequency',valFrequency, ...
    'Verbose',false, ...
    'Plots','training-progress');
net = trainNetwork(imdsTrain,lgraph,options);
%%


tic
feat_tr = [];
labels_tr = [];
sizeDataStore = size(imdsTrain.Files);
fileTrain = imdsTrain.Files;
labelTrain = imdsTrain.Labels;
uniqueLabel = unique(labelTrain);
for j = 1:length(uniqueLabel)
    class = string(uniqueLabel(j));
    disp(class + "("+j+")");
    subtable = fileTrain(labelTrain == string(class));
    [inst,dimension] = size(subtable);
    for i = 1: inst
        im = im2double(imread(subtable{i}));
        feat_tmp=activations(net,im,'new_classoutput','OutputAs','rows','ExecutionEnvironment', 'cpu');
        singFeat = gather(feat_tmp);
        feat_tr=[feat_tr; singFeat];
        labels_tr=[labels_tr; class];
    end
end
toc

%% Similarity tramite fine tuning per test
tic
feat_te = [];
labels_te = [];
fileTest = imdsTest.Files;
for i = 1: length(fileTest)
    name = fileTest(i);
    name = name{1};
    im = im2double(imread(name));
    feat_tmp=activations(net,im,'new_classoutput','OutputAs','rows', 'ExecutionEnvironment', 'cpu');
    singFeat = gather(feat_tmp);
    feat_te=[feat_te; singFeat];
    labels_te=[labels_te; class];
end
toc

%% Stampa similarità
allFiles = imdsTrain.Files;
D=pdist2(feat_te,feat_tr, 'cosine');
for i = 1: length(fileTest)
    nameIm = fileTest(i);
    nameIm = nameIm {1};
    im = imread(nameIm);
    figure(1);
    subplot(2,3,1);
    imshow(im);
    [B, I] = mink(D(i,:),5);
    for j = 1:5
        nameIm = allFiles(I(j));
        nameIm = nameIm{1};
        imMostSim = imread(nameIm);
        subplot(2,3,j+1);
        imshow(imMostSim);
        saveas(gcf,'output/figure'+string(i)+'_sim.jpg');
    end 
end