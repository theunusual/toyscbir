%% Setting variabili per esecuzione file
locPython = "C:\Users\ilbro\anaconda3\python.exe"; % Path Python
%% Generazione sottocartelle per newTrain
disp("Generazione sottocartelle");
imdsAll = imageDatastore(fullfile('train'),'FileExtensions','.jpg', 'LabelSource', 'foldernames', 'IncludeSubfolders', true);
allLabels = imdsAll.Labels;
for i = 1:length(allLabels)
    newFolder = strcat("newTrain/", string(allLabels(i)));
    if ~exist(newFolder, 'dir')
       mkdir(newFolder);
    end
end
%% Generazione bounding box per train
disp("Generazione bounding box per train")
locFile = strcat('"',pwd, '\U-2-Net-master\u2net_test.py','"');
commandStr = strcat(locPython, " ", locFile);
system(commandStr)
%% Generazione  per test
disp("Generazione bounding box per test")
locFile = strcat('"',pwd, '\U-2-Net-master\u2net_testNew.py','"');
commandStr = strcat(locPython, " ", locFile);
system(commandStr)