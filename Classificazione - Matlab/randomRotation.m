function angRot = randomRotation()
    rNum = randi([1,2],1);
    if rNum == 1
        angRot = 0;
    else
        if rNum == 2
            angRot = 90;
        end
    end
    rNum = randi([-15 15],1);
    angRot = angRot + rNum;
end