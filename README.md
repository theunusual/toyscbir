IMPORTANTE
PER PRIMA COSA, UNZIPPARE LA CARTELLA DATASETMASK PRESENTE SU DRIVE NELLA CARTELLA TOYSCBIR/BOT E SALIENCY - PYTHON

Istruzioni varie

Per calcolare la saliency su tante immagini insieme:
- Aprire il file Bot e saliency - Python/povaMaschere.py 
- Settare image_dir con la cartella da cui prendere le immagini
- Settare prediction_dir con la cartella in cui salvare le maschere. Importante: se volete salvarle in una cartella
chiamata "mask" create al suo interno una sottocartella chiamata train e settate il percorso a mask/train
- Eseguire il file Bot e saliency - Python/povaMaschere.py 

Per estrarre gli oggetti dalla saliency ottenuta:
- Eseguire il file Bot e saliency - Python/extractBB.m

Per far funzionare il bot:
- Eseguire il file Bot e saliency - Python/myBot.py

