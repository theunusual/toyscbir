clear allageTrain.Files;
clc
close all

%% carico la rete
net = resnet50;
%analyzeNetwork(net);
sz=net.Layers(1).InputSize;
layer='avg_pool'; 
load("feature/avgpool_gray_obj.mat")
%% estrazione delle features di training
feat_tr=[];
tic
imageTrain = imageDatastore(fullfile('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\toyscbir\Bot e saliency - Python\Tutte le maschere\object'),'FileExtensions','.png');
fileTrain = imageTrain.Files;
tic
for i = 1:length(fileTrain)
        disp(i);
        im=double(imread(fileTrain{i}));
        im=imresize(im,sz(1:2));
        im = gpuArray(im);
        feat_tmp=activations(net,im,layer,'OutputAs','rows', 'ExecutionEnvironment', 'gpu', 'Acceleration', 'mex');
        feat_tr=[feat_tr; feat_tmp];
end
toc
feat_tr = gather(feat_tr);

%% estrazione delle features di test
feat_te=[];
tic
imOrig=imread('im.jpg');
bw = imread('bw.jpg');
bw = bw(:,:,1);
bw = bw > 10;
bw = bwareaopen(bw, 2000);
bw = imfill(bw, 'holes');
obj = double(imOrig).*  bw;
info = regionprops(bw,'Boundingbox');
BB = info(1).BoundingBox;
onlyObject = imcrop(obj, BB);
x=imresize(onlyObject,sz(1:2));
feat_tmp=activations(net,x,layer,'OutputAs','rows');
feat_te=[feat_te; feat_tmp];
toc

%% feature normalization
disp("normalizzo");
feat_tr=feat_tr./sqrt(sum(feat_tr.^2,2));
feat_te=feat_te./sqrt(sum(feat_te.^2,2));

%% classificatore con 1-KK <-------
[Idx,D] = knnsearch(feat_tr, feat_te, 'K', 11, 'Distance', 'minkowski');
imageTrain = imageDatastore(fullfile('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\toyscbir\Bot e saliency - Python\Tutte le maschere\object'),'FileExtensions','.png');
fileTrain = imageTrain.Files;
figure(1), clf
subplot(3,4,1), imshow(uint8(onlyObject))
title('Immagine originale');
for i = 1:11
    name =  fileTrain(Idx(i));
    name = name {1};
    objIm = imread(name);
    subplot(3,4,i+1), imshow(objIm)
    title(string(name(169:175)));
end
    
