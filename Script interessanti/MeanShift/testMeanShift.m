clc
close all
warning off
rgbImage=imread('../83696.jpg');
subplot(1,3,1);
imshow(rgbImage);
title('Original Image');
ax=gca;
ax.TitleFontSizeMultiplier=2;
redChannel=rgbImage(:, :, 1);
greenChannel=rgbImage(:, :, 2);
blueChannel=rgbImage(:, :, 3);
data=double([redChannel(:), greenChannel(:), blueChannel(:)]);
[n,m,not_required] = MeanShiftCluster(data',100,0);
n=n';
m=reshape(m',size(rgbImage,1),size(rgbImage,2));
n=n/255;
clusteredImage=label2rgb(m,n);
subplot(1,3,2);
imshow(clusteredImage);
title('High Radius')
