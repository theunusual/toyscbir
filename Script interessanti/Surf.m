clear
close all

%% caricamento e scorrimento file train
%imageTrain = imageDatastore(fullfile('C:\Users\raffa\Desktop\Visual Information Procesdsing and Management\progetto\train'),'FileExtensions','.jpg');
%fileTrain = imageTrain.Files;
%for i = 1:length(fileTrain)
%    disp(i)
    %im = im2double(imread(fileTrain{i}))
%    im = imread(fileTrain{i});
%    imshow(im);
%end


%% surf immagine perfetta 83751
% immagine esempio (template)
boxImage = rgb2gray(imread('./train/83703.jpg'));
% immagine scena
sceneImage = rgb2gray(imread('./train/83703.jpg'));
%% estrazione punti notevoli e visualizzazione
boxPoints = detectSURFFeatures(boxImage);
scenePoints = detectSURFFeatures(sceneImage);

figure(1),clf
imshow(boxImage), hold on
plot(selectStrongest(boxPoints,100)), hold off

figure(2),clf
imshow(sceneImage), hold on
plot(selectStrongest(scenePoints,100)), hold off

%% chiamata al descrittore

[boxFeatures,boxPoints]=extractFeatures(boxImage,boxPoints);
[sceneFeatures,scenePoints]=extractFeatures(sceneImage,scenePoints);

%% match tra features

boxPairs = matchFeatures(boxFeatures,sceneFeatures);
matchedBoxPoints=boxPoints(boxPairs(:,1),:);
matchedScenePoints=scenePoints(boxPairs(:,2),:);

figure(3),clf
showMatchedFeatures(boxImage,sceneImage,matchedBoxPoints,matchedScenePoints,'montage');

%% ricerca delle immagini con il maggior numero di match
boxImage = rgb2gray(imread('./train/83726.jpg'));
boxPoints = detectKAZEFeatures(boxImage);
[boxFeatures,boxPoints]=extractFeatures(boxImage,boxPoints);

imageTrain = imageDatastore(fullfile('C:\Users\raffa\Desktop\Visual Information Procesdsing and Management\progetto\train'),'FileExtensions','.jpg');
fileTrain = imageTrain.Files;
matchResult = [];
tic
for i = 1:length(fileTrain)
    disp(i);
    sceneImage = rgb2gray(imread(fileTrain{i}));
    scenePoints = detectKAZEFeatures(sceneImage);
    [sceneFeatures,scenePoints]=extractFeatures(sceneImage,scenePoints);
    boxPairs = matchFeatures(boxFeatures,sceneFeatures);
    matchResult = [matchResult; length(boxPairs)];
end
toc

%% elaborazione risultati
tmp = matchResult;
tmp = sort(tmp);
imResult = [];
maxvalue = tmp(length(tmp)-4:length(tmp));
for i = 1:5
   u = find(matchResult == maxvalue(i));
   imResult = [imResult; u+83685];
end

% surf: trova corrispondenze, non molto preciso
% kaze: non so cosa fa, ma fatto girare sulle prime 91 immagini (lento) mi
%       trova 3 corrispondenze interessanti
%