function downloadTrainedNIMANet(url,destination)
% The downloadTrainedNIMANet function downloads the pretrained NIMA network
% that quantifies image quality.
%
% Copyright 2020 The MathWorks, Inc.

[~,name,~] = fileparts(url);
netDirFullPath = destination;
netZipFileFullPath = fullfile(destination,[name '.zip']);
netMATFileFullPath = fullfile(destination,[name '.mat']);
if ~exist(netMATFileFullPath,'file')
    if ~exist(netZipFileFullPath,'file')
        fprintf('Downloading pretrained NIMA network.\n');
        fprintf('This can take several minutes to download...\n');
        if ~exist(netDirFullPath,'dir')
            mkdir(netDirFullPath);
        end
        websave(netZipFileFullPath,url);
        fprintf('Done.\n\n');
    end
    unzip(netZipFileFullPath,netDirFullPath)
else
    fprintf('Pretrained NIMA network already exists.\n\n');
end
end