%NOTA: MODIFICARE I PATH

im = imread('/MATLAB Drive/83700.jpg');
vectorH= imhist(im);
skewns = (sum((vectorH-mean(vectorH)).^3)./length(vectorH)) ./ (var(vectorH,1).^1.5);
imshow(im);
%% se l'immagine è scura allora applico bilaterale per adattare chiaro-scuro
contaSx = 0;
contaDx = 0; 
if skewns > 5.0
    for i = 1: size(vectorH,1)
       if i < (size(vectorH,1)/2)
           contaSx = contaSx + vectorH(i);
       else
           contaDx = contaDx + vectorH(i);
       end
    end
end
if contaSx > contaDx
    perc = (contaDx+contaSx)\contaDx*100;
    if perc<25
        imYCbCr = rgb2ycbcr(im);
        canaleY = imYCbCr(:,:,1);
        % Applico un filtro bilaterale per ottenere la maschera per la gamma
        mask=imbilatfilt(canaleY,25,25);
        % Inversa della maschera
        mask = 255 - mask;
        % Mettiamo in double immagine e maschera (attenzione: non tra 0 e 1, ma tra 0 e 255 con virgola)
        canaleY=double(canaleY);
        mask=double(mask);
        % Formula per la gamma adattativa
        gamma=1.5.^((128-mask)/128);
        nuovoCanaleY=255*(canaleY/255).^gamma;
        % Trasformo il canale calcolato in uint8 e lo rimetto nell'immagine
        imYCbCr(:,:,1) = uint8(nuovoCanaleY);
        % Riporto l'immagine nello spazio RGB
        imRis = ycbcr2rgb(imYCbCr);
        % Si può notare come l'immagine risultante sia poco saturata. A tal fine spazio HSV sarebbe 
        % ottimo, ma visto che siamo in YCbCr possiamo lavorare sulle componenti CbCr
        imYCbCr(:,:,2:3)=uint8((double(imYCbCr(:,:,2:3)-127)*1.2)+127);
        imRisMoreSat = ycbcr2rgb(imYCbCr);
        figure(1);
        subplot(1, 3, 1), imshow(im), title ('Immagine originale');
        subplot(1, 3, 2), imshow(imRis), title ('Applicazione gamma correction adattativa');
        %subplot(1, 3, 3), imshow(imRisMoreSat), title ('Applicazione gamma correction adattativa e aumento saturazione');
        % Confronto tra maschere e risultati ottenuti con filtro gaussiano e bilaterale
        figure(2);
        subplot(1,2,1), imshow(uint8(mask)), title('Maschera filtro bilaterale');
        %subplot(1,2,2), imshow(uint8(maskGauss)), title('Maschera filtro gaussiano');
        figure(3);
        subplot(1,2,1), imshow(imRis), title('Utilizzo filtro bilaterale');
        %subplot(1,2,2), imshow(imRisGauss), title('Utilizzo filtro gaussiano');
        imwrite(imRis, "C:\Users\alebo\OneDrive\Desktop\MAGISTRALE\SECONDO ANNO\VISUAL INFORMATION PROCESSING AND MANAGEMENT\IMG.jpg")
    end
end
