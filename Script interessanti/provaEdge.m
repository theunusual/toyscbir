close all
im = rgb2gray(imread('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\toyscbir\Dataset\train\83696.jpg'));
%% ciao
val = 0.3;
BWCanny = edge(im, 'Canny', [0 val]);
figure, imshow(BWCanny);
BWSobel = edge(im, 'Sobel', 1);
figure, imshow(BWSobel);
BWPrewitt = edge(im, 'Prewitt', val);
figure, imshow(BWPrewitt);
BWRoberts = edge(im, 'Roberts', val);
figure, imshow(BWRoberts);
BWlog = edge(im, 'log', val);
figure, imshow(BWlog);
BWzerocross = edge(im, 'zerocross', val);
figure, imshow(BWzerocross);

%% Boundaries
BW = imbinarize(im);
B = bwboundaries(BW);
imshow(im)
hold on;
imRis = zeros([1334 1000]);
hold on;
for k=1:length(B)
   b = B{k};
   plot(b(:,2),b(:,1),'g','LineWidth',1);
end

BW2 = bwperim(BW);
imshow(BW2);

%% Gradiente
[Gmag, Gdir] = imgradient(im,'prewitt');
figure
imshowpair(Gmag, Gdir, 'montage');
[Gx,Gy] = imgradientxy(im, 'prewitt');
figure
imshowpair(Gx, Gy, 'montage');

%% block
S = qtdecomp(im);
blocks = repmat(uint8(0),size(S));

for dim = [512 256 128 64 32 16 8 4 2 1]   
  numblocks = length(find(S==dim));    
  if (numblocks > 0)        
    values = repmat(uint8(1),[dim dim numblocks]);
    values(2:dim,2:dim,:) = 0;
    blocks = qtsetblk(blocks,S,dim,values);
  end
end

blocks(end,1:end) = 1;
blocks(1:end,end) = 1;

imshow(im)