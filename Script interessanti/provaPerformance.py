# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 18:35:12 2021

@author: alebo
"""

import os
from skimage import io, transform
import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms#, utils
# import torch.optim as optim
import pandas as pd
import numpy as np
from PIL import Image
import glob
import cv2
from data_loader import RescaleT
from data_loader import CenterCrop
from data_loader import ToTensor
from data_loader import ToTensorLab
from data_loader import SalObjDataset

from model import BASNet
from model.u2net import U2NET
import subprocess

''' lettura dataset etichette di train'''
data = pd.read_csv("../Dataset/trainLabel.csv") 

def normPRED(d):
	ma = torch.max(d)
	mi = torch.min(d)

	dn = (d-mi)/(ma-mi)

	return dn

def save_output(image_name,pred,d_dir):
    
	predict = pred
	predict = predict.squeeze()
	predict_np = predict.cpu().data.numpy()

	im = Image.fromarray(predict_np*255).convert('RGB')
	img_name = image_name.split("/")[-1]
	image = io.imread(image_name)
	imo = im.resize((image.shape[1],image.shape[0]),resample=Image.BILINEAR)
	'''imo = np.array(imo)
	imo = cv2.bitwise_and(image, imo)
	imo = Image.fromarray(imo)'''
	aaa = img_name.split(".")
	bbb = aaa[0:-1]
	imidx = bbb[0]
	for i in range(1,len(bbb)):
		imidx = imidx + "." + bbb[i]

	imo.save(d_dir+imidx+'.png')


if __name__ == '__main__':
	# --------- 1. get image path and name ---------
	
	image_dir = './../Dataset/test/'
	prediction_dir = './datasetMask/'
	model_dir = './saved_models/basnet_bsi/basnet.pth'
    
	
	
	img_name_list = glob.glob(image_dir + '*.jpg')
	
	# --------- 2. dataloader ---------
	#1. dataload
	test_salobj_dataset = SalObjDataset(img_name_list = img_name_list, lbl_name_list = [],transform=transforms.Compose([RescaleT(256),ToTensorLab(flag=0)]))
	test_salobj_dataloader = DataLoader(test_salobj_dataset, batch_size=1,shuffle=False,num_workers=1)
	
	# --------- 3. model define ---------
	print("...load BASNet...")
	net = BASNet(3,1)
	net.load_state_dict(torch.load(model_dir))
	if torch.cuda.is_available():
		net.cuda()
	net.eval()
	'''print("...load U2NET---173.6 MB")
	net = U2NET(3,1)
	if torch.cuda.is_available():
		net.load_state_dict(torch.load(model_dir))
		net.cuda()
	else:
		net.load_state_dict(torch.load(model_dir, map_location='cpu'))
	net.eval()'''
	# --------- 4. inference for each image ---------
	for i_test, data_test in enumerate(test_salobj_dataloader):
	
		print("inferencing:",img_name_list[i_test].split("/")[-1])
	
		inputs_test = data_test['image']
		inputs_test = inputs_test.type(torch.FloatTensor)
	
		if torch.cuda.is_available():
			inputs_test = Variable(inputs_test.cuda())
		else:
			inputs_test = Variable(inputs_test)
	
		d1,d2,d3,d4,d5,d6,d7, d8 = net(inputs_test)
	
		# normalization
		pred = d1[:,0,:,:]
		pred = normPRED(pred)
	
		# save results to test_results folder
		save_output(img_name_list[i_test],pred,prediction_dir)
	
		del d1,d2,d3,d4,d5,d6,d7,d8
        
	cur_dir = os.path.dirname(os.path.realpath(__file__))
  		
	matlab_cmd = '"C:\\Program Files\\MATLAB\\R2021a\\bin\\matlab.exe"'
	cmd = matlab_cmd + " -nodesktop -nosplash -wait -r \"addpath(\'" + cur_dir + "\'); testSimilarity(); quit\""
	print(cmd)
	subprocess.call(cmd,shell=True)
    
	''' nel file avrò ogni 12 a partire da 0 (modulo 12), il file testato da cui estrarre la classe, poi delle altre 11 cerco la loro classe e a quel punto calcolo precision e recall della query 1...e cosi per ogni query di test'''
	ind = 0
	tot = 0
	corretti = 0
	with open('risultati.txt') as f:
		for line in f:
			tot = tot + 1
			line = line[0:len(line)-1]
			if ind % 12 == 0: 					
				row = data.loc[data['name']==line]
				classe = row["class"].values
				classe = classe[0]
				print(classe)
			else:
				row = data.loc[data['name']==line]
				classeQ = row["class"].values
				classeQ = classeQ[0]
				print("query result", line, classeQ)
				if(classeQ == classe):
					corretti = corretti + 1
			ind = ind + 1
			if 'str' in line:
				break
        
	
	precision = corretti/tot
    
	''' NOTA: nel file di matlab testSimilarity forse è meglio NON contare le immagini con 2 o + oggetti. Quindi labelling e conta regioni con funzione max'''
	'''recall = corretti/(il numero di immagini con classe == alla classe dell'immagine di test se queste sono minori di 11, altrimenti 11	'''