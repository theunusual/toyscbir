close all;
im = im2double(imread('C:\Users\ilbro\Documents\MEGA\Università\Magistrale\Secondo anno\Visual information processing and management\toyscbir\Dataset\train\83696.jpg'));
gray = rgb2gray(im);
figure,imshow(gray);
patchVar = std2(im)^2;
DoS = 2*patchVar;
J = imbilatfilt(gray,DoS, 2);

J = medfilt2(gray,[10 10]);

J = double(edge(J,'Canny',0.5));
figure,imshow(J);
highpass = gray - J;
figure, imshow(highpass);
highContr = gray + highpass;
figure, imshow(highContr);
imwrite(highContr, 'risDIno2.png');
