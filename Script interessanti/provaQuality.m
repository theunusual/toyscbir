imageDir = fullfile(pwd,"LIVEInTheWild");
if ~exist(imageDir,'dir')
    mkdir(imageDir);
end
trainedNIMA_url = 'https://ssd.mathworks.com/supportfiles/image/data/trainedNIMA.zip';
downloadTrainedNIMANet(trainedNIMA_url,imageDir);
load(fullfile(imageDir,'trainedNIMA.mat'));
%% Prova NIMA
im = imread('Dataset/train/83721.jpg');
[meanOriginal,stdOriginal] = predictNIMAScore(dlnet,im);
figure
t = tiledlayout(1,1);
displayImageAndScoresForNIMA(t,im,meanOriginal,stdOriginal,"Original Image")

%% Prova PIQE
imageBw = imageDatastore('Dataset/train','FileExtensions','.jpg');
allFile = imageBw.Files;
for i = 1:length(allFile)
    im = imread(strcat('Dataset\train\',string(i + 83685), '.jpg'));
    [score,activityMask,noticeableArtifactsMask,noiseMask] = piqe(im);
    [meanOriginal,stdOriginal] = predictNIMAScore(dlnet,im);
    mask_1 = labeloverlay(im,activityMask,'Colormap','winter','Transparency',0.25);
    mask_2 = labeloverlay(im,noticeableArtifactsMask,'Colormap','autumn','Transparency',0.25);
    mask_3 = labeloverlay(im,noiseMask,'Colormap','hot','Transparency',0.25);
    montage({im,mask_1,mask_2,mask_3},'Size',[1 4])
    title(strcat('Original Image (Piqe = ',num2str(score),', Mean =', num2str(meanOriginal), ', Std = ', num2str(stdOriginal), ')    |    Overlay activityMask    |    Overlay noticeableArtifactsMask    |    Overlay noiseMask'),'FontSize',12)
    saveas(gcf, strcat('similarity/', string(i+83685), '.png'));
end