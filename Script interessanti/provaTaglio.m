clear all; %*bold*
close all;
clc;
fontSize = 13;
rgbImage = imread('83696.jpg');
[row, col, ch] = size(rgbImage);
rowQuart = floor(row/4);
colQuart = floor(col/4);
rowCenter = floor(row/2);
colCenter = floor(col/2);
imshow(rgbImage);
axis on
hold on;
rect = rectangle('Position',[colCenter-colQuart,rowCenter-rowQuart,colQuart*2,rowQuart*2],...
         'LineWidth',2,'LineStyle','--','EdgeColor', 'r',...
  'LineWidth', 3);
hold off;
Icropped = imcrop(rgbImage,rect.Position);

imwrite(Icropped, 'center.png');