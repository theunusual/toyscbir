trainSet = readtable('trainLabel.csv');
mkdir("newTrain")
for class = 5814:6776
    mkdir("newTrain/" + class)
    subtable = trainSet(trainSet.class == class,:);
    [inst,dimension] = size(subtable);
    for i = 1: inst
        nameFile = strcat('train/',char(subtable{i, 1}));
        movefile(nameFile,'newTrain/' + string(class))
    end
end