fileName = '83696';
A=imread(strcat('image/input/', fileName, '.jpg'));
bw = imread(strcat('image/mask/', fileName, '.png'));
bw = bw(:,:,1);
bw = bw > 10;
bw = bwareaopen(bw, 2000);
bw = imfill(bw, 'holes');
[L,N] = superpixels(A,250);
BW = boundarymask(L);
outputImage = zeros(size(A),'like',A);
idx = label2idx(L);
numRows = size(A,1);
numCols = size(A,2);
for labelVal = 1:N
    redIdx = idx{labelVal};
    greenIdx = idx{labelVal}+numRows*numCols;
    blueIdx = idx{labelVal}+2*numRows*numCols;
    outputImage(redIdx) = mean(A(redIdx));
    outputImage(greenIdx) = mean(A(greenIdx));
    outputImage(blueIdx) = mean(A(blueIdx));
end    

figure
imshow(outputImage);
ris = activecontour(outputImage,bw,1000, 'Chan-vese');
figure, imshow(im2double(A) .* bw);
figure, imshow(im2double(A) .* ris);